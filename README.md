# GraphQL Query Builder

<center>
⚠️ This repo has been moved to <a href="https://gitlab.com/DrakeTDL/graphql_query_builder">here</a> ⚠️
</center>

---

This is a GraphQL query builder.  

## Install

``` sh
cargo add graphql_query_builder
```

## Implementation Checklist

- [x] Basic queries/mutations/subscriptions
- [ ] variables
- [X] arguments
- [X] Field aliases
- [ ] [Fragments](https://spec.graphql.org/October2021/#sec-Language.Fragments)
- [X] Union (Inline Fragment)
- [ ] [Directives](https://spec.graphql.org/October2021/#sec-Language.Directives)
- [ ] [TypeCondition](https://spec.graphql.org/October2021/#TypeCondition)
- [ ] ???