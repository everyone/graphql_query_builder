pub mod input_values {
    #[derive(Clone, Debug)]
    pub enum Values {
        Int(isize),
        Float(f64),
        String(&'static str),
        Boolean(bool),
        Null,
        Enum(Vec<&'static str>),
        List(Vec<isize>),
        Object,
    }
    impl core::convert::From<bool> for Values {
        fn from(value: bool) -> Self {
            Self::Boolean(value)
        }
    }
    impl core::convert::From<f64> for Values {
        fn from(value: f64) -> Self {
            Self::Float(value)
        }
    }
    impl core::convert::From<isize> for Values {
        fn from(value: isize) -> Self {
            Self::Int(value)
        }
    }
    impl core::convert::From<&'static str> for Values {
        fn from(value: &'static str) -> Self {
            Self::String(value)
        }
    }
    impl<const N: usize> core::convert::From<[isize; N]> for Values {
        fn from(value: [isize; N]) -> Self {
            Values::List(Vec::from(value))
        }
    }
    impl<const N: usize> core::convert::From<[&'static str; N]> for Values {
        fn from(value: [&'static str; N]) -> Self {
            Values::Enum(Vec::from(value))
        }
    }
}

#[derive(strum_macros::IntoStaticStr)]
#[strum(serialize_all = "snake_case")]
pub enum OperationType {
    Query,
    Mutation,
    Subscription,
}
