mod enums;
pub mod json;
pub use enums::{input_values::Values, OperationType};
use std::collections::HashMap;

type Fields = Vec<SelectionSet>;
type Arguments = HashMap<&'static str, Values>;

#[derive(Clone, Debug)]
pub struct SelectionSet {
    pub operation: &'static str,
    pub alias: Option<&'static str>,
    pub fields: Option<Fields>,
    pub arguments: Option<Arguments>,
    pub is_union: bool,
}

pub fn tabs(level: usize) -> String {
    "\t".repeat(level)
}

pub struct Builder;

impl Builder {
    pub fn new(operation: OperationType, selection_set: &SelectionSet) -> String {
        Self::generate_field(
            0,
            SelectionSet {
                operation: operation.into(),
                alias: None,
                fields: Some(vec![selection_set.clone()]),
                arguments: None,
                is_union: false,
            },
        )
    }

    fn parse_arguments(args: &Arguments) -> Vec<String> {
        args.into_iter()
            .map(|(key, value)| match value {
                Values::Int(value) => format!("{}: {}", key, value),
                Values::Float(_) => todo!(),
                Values::String(value) => format!("{}: \"{}\"", key, value),
                Values::Boolean(value) => format!("{}: {}", key, value),
                Values::Null => format!("{}: null", key),
                Values::Enum(value) => format!("{}: {}", key, value.join(", ")),
                Values::List(value) => format!(
                    "{}: {}",
                    key,
                    value
                        .iter()
                        .map(|i| i.to_string())
                        .collect::<Vec<String>>()
                        .join(", ")
                ),
                Values::Object => todo!(),
            })
            .collect()
    }

    fn generate_field(level: usize, field: SelectionSet) -> String {
        let tabs = tabs(level);
        let operation = field.operation;
        let union = if field.is_union { "... on " } else { "" };
        let alias = if let Some(alias) = field.alias {
            format!("{}: ", alias)
        } else {
            String::from("")
        };
        let arguments = if let Some(args) = field.arguments {
            format!("({})", Self::parse_arguments(&args).join(", "))
        } else {
            String::from("")
        };
        let sub_field = if let Some(sf) = field.fields {
            let sub_field = Self::map_fields(level, sf).join("\n");
            format!(" {{\n{sub_field}\n{tabs}}}")
        } else {
            String::from("")
        };
        format!("{tabs}{union}{alias}{operation}{arguments}{sub_field}")
    }
    fn map_fields(level: usize, selection_sets: Fields) -> Vec<String> {
        selection_sets
            .clone()
            .into_iter()
            .map(|field| Self::generate_field(level + 1, field))
            .collect()
    }
}
