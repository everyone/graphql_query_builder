#[macro_export]
macro_rules! json {
    ({$($key:literal: $value:tt),+ $(,)?}) => {
      Some(std::collections::HashMap::from([$(($key, $crate::Values::from($value)),)+]))
    };
}
