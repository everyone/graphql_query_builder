use graphql_query_builder::{json, Builder, OperationType, SelectionSet};

pub fn query() -> String {
    let story = SelectionSet {
        operation: "Story",
        alias: None,
        fields: Some(vec![SelectionSet {
            operation: "likeCount",
            alias: None,
            fields: None,
            arguments: None,
            is_union: false,
        }]),
        arguments: None,
        is_union: false,
    };
    let like_story = SelectionSet {
        operation: "likeStory",
        alias: None,
        fields: Some(vec![story]),
        arguments: json!({ "storyID": 12345 }),
        is_union: false,
    };
    Builder::new(OperationType::Mutation, &like_story)
}

fn main() {
    println!("{}", query());
}
