use graphql_query_builder::{json, Builder, OperationType, SelectionSet};

pub fn query() -> String {
    let vec = vec![
        SelectionSet {
            operation: "id",
            alias: None,
            fields: None,
            arguments: None,
            is_union: false,
        },
        SelectionSet {
            operation: "name",
            alias: None,
            fields: None,
            arguments: None,
            is_union: false,
        },
        SelectionSet {
            operation: "profilePic",
            alias: Some("smallPic"),
            fields: None,
            arguments: None,
            is_union: false,
        },
        SelectionSet {
            operation: "profilePic",
            alias: Some("bigPic"),
            fields: None,
            arguments: None,
            is_union: false,
        },
    ];
    let user = SelectionSet {
        operation: "user",
        alias: None,
        fields: Some(vec),
        arguments: json!({ "id": 4 }),
        is_union: false,
    };
    Builder::new(OperationType::Query, &user)
}

fn main() {
    println!("{}", query());
}
